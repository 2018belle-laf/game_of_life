## Fonctionnalité 7
from matplotlib.animation import FuncAnimation


"""
def gen_data(): #on génère notre univers
    global univers_evol
    univers_evol=generation(univers_evol)
    return(univers_evol)
        
def update(data): 
    mat.set_data(data)
    return(mat)
        
def data_gen():#on fait tourner l'algo
    while True: 
        yield gen_data()
        
fig,ax=plt.subplots()
univers_evol=game_life_simulate(generate_universe_v2(),0)  #on utilise notre fonction qui lance le jeu de la vie
mat=ax.matshow(gen_data(),cmap="gray_r") #on fait l'affichage  
anim=FuncAnimation(fig,update,data_gen,interval=500) #on anime l'image
plt.show()
"""

import imageio
def gif(liste_image, addresse):
    images = [] #va contenir les images pour le module imageio
    for filename in liste_image:
        images.append(imageio.imread(filename))#On ajoute les images dans la liste dédiée
    imageio.mimsave(addresse, images)#On enregistre le gif.


    
## Fonctionnalité 8 version 3
import imageio
def generate_universe_v3(tuple, key , seed_position):
    """Tuple est un tuple correspondant aux dimensions de l'univers. key est une str correspondant à la graine initiale. seed_position donne les coordonnées du point en haut à gauche de la graine."""
    print(seeds.keys()) #l'utilisateur voit le catalogue disponible
    seed=np.array(seeds[key])#on récupère la graine.
    univers=generate_universe(tuple) #on génère l'univers
    x_start = seed_position[0]
    y_start = seed_position[1]
    univers=add_seed_to_universe(seed,univers,x_start,y_start) #on ré-utilise la fonction add... en plaçant notre nouvelle graine
    return(univers)
    
def generation_v2 (univers):
    new_univers = univers.copy() #On conserve la génération précédente.
    for x_coord in range(univers.shape[0]):
        for y_coord in range(univers.shape[1]):
            new_univers[x_coord][y_coord]=survival(x_coord,y_coord,univers) #On remplit cellule par cellule la nouvelle génération.
    return(new_univers)

def affichage_bis(tuple, key , seed_position,dt=300,color='gray_r', iter = 30, Gif = False):
    """Tuple est un tuple correspondant aux dimensions de l'univers. key est une str correspondant à la graine initiale. seed_position donne les coordonnées du point en haut à gauche de la graine. dt, int, donne en milliseconde l'intervalle entre deux updates. color est une str donnant la couleur des cases. iter, int, donne le nombre d'itérations du processus. Gif est un booléen qui détermine si le programme va donne un GIF ou non. PS : cette dernière fonction n'est actuellement pas valide."""
    fig = plt.figure() #on génère la figure.
    univers = generate_universe_v3(tuple, key , seed_position) #On crée l'univers initial.
    image = [] #image regroupe tous les états successifs (but : créer le GIF)
    for gene in range(iter): #On itère le passage des générations.
        frame = plt.imshow(univers,cmap=color) #On affiche la grille.
        plt.pause(dt/1000) #On met le programme en pause pour voir la grille.
        univers = generation_v2(univers) #On fait progresser le processus.
        image.append(frame) #On ajoute cet état à la liste des états.
    if Gif:
        gif(image,'/Users/Théo/Documents.gif') #Création du gif
    plt.show()
    
##fonctionnalité 9 (nous sommes encore en train de chercher)

## fonctionnalité 10
"""
if __name__=='__main__':
    size=(int(input("nb lignes ?")),int(input("nb colonnes ?")))
    print(seeds.keys()) #l'utilisateur voit le catalogue disponible
    key=input("écrivez une clef en minuscules") #il saisit sa clef
    seed_position=(int(input("ligne de la seed?")),int(input("colonne de la seed?")))
    affichage(size,key,seed_position)
    """