#Fonction de jeu en pygame   

from gameoflife_tg_fbl_partie1_matplotlib import*
from gameoflife_tg_fbl_partie2_matplotlib import*
from gameoflife_tg_fbl_partie_tkinter import*


     
def play():                
        #initialization
        pygame.init()
        scrn = pygame.display.set_mode((500, 500))#on ouvre une fenêtre de la taille souhaitée
        mainsrf = pygame.Surface((500, 500))
        mainsrf.fill(white) #on prend un fond blanc
        univers=generate_universe_v2()#on initialise notre univers à l'aide de la fonction que nous avons créée, on doit juste prendre le temps dans la console de spécifier la seed désirée
        
        while 1:
                univers=generation_v2(univers)# on update grâce à notre fonction l'univers de jeu
                #boucle préventive qui gère le cas où l'utilisateur souhaite quitter le jeu
                for event in pygame.event.get():
                        if event.type == QUIT:
                                pygame.quit()
                                """sys.exit()""" #permet d'éviter d'avoir kernel process exited 
                #partie traçage de l'algorithme 
                for y in range(univers.shape[0]):
                        for x in range(univers.shape[1]):
                                if univers[x][y]==1: #si cellule vivante
                                        pygame.draw.rect(mainsrf, red, (x*10, y*10, 10, 10))
                                else:#si morte
                                        pygame.draw.rect(mainsrf, white, (x*10, y*10, 10, 10))
                scrn.blit(mainsrf, (0, 0))
                pygame.display.update()
