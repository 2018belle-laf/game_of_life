from pytest import *
import numpy as np
import matplotlib.pyplot as plt


def test_generate_universe():
    condition=True
    grille_expected=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]])
    grille_test=generate_universe((4,4))
    for line in range(4):
        for column in range(4):
            if grille_expected[line,column]!=grille_test[line,column]:
                condition=False
    return(condition)


def test_add_seed_to_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed, universe,x_start=1, y_start=1)
    test_equality=np.array(universe ==np.array([[0,0, 0, 0, 0, 0],
 [0, 0, 1, 1, 0, 0],
 [0, 1, 1, 0, 0, 0],
 [0 ,0, 1, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0]],dtype=np.uint8))
    assert test_equality.all()
    
def test_generation ():
    universe = np.array([[0,0,0,0,0,0],[0,0,0,1,0,0],[0,0,0,0,0,0],[0,0,0,0,1,1],[0,0,0,0,1,0],[0,0,0,0,0,0]])
    new_universe = np.array([[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,1,0],[0,0,0,0,1,1],[0,0,0,0,1,1],[0,0,0,0,0,0]]) #On génère une cellule, on en détruit une et on génère une cellule sur le bord.
    test_universe = generation(universe)
    return((new_universe == test_universe).all())

def test_game_life_simulate ():
    universe = np.array([[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,0,0,0,0],[0,0,0,0,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]]) #On génère un beacon.
    new_universe = np.array([[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,1,0,0,0],[0,0,0,1,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]]) #Si le nombre d'itération est impaire, comme c'est un oscillateur d'ordre deux, on aura un autre motif.
    test_universe = game_life_simulate(universe,11)#Ici, le test doit être le nouveau motif.
    return((new_universe == test_universe).all())