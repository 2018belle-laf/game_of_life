import numpy as np
import matplotlib.pyplot as plt

## test_generate_universe


from pytest import *


def test_generate_universe():
    condition=True
    grille_expected=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]])
    grille_test=generate_universe((4,4))
    for line in range(4):
        for column in range(4):
            if grille_expected[line,column]!=grille_test[line,column]:
                condition=False
    return(condition)
    
## generate_universe

def generate_universe(size): #size est un tuple
    universe=np.zeros(size)
    return(universe)
    
## test pentomino

def test_create_seed(): #nous n'avions pas compris à l'époque la méthode .all()
    
    condition=True
    grille_expected=np.array([[0, 1, 1], [1, 1, 0], [0, 1, 0]])
    grille_test=create_seed(type_seed="r_pentomino")
    for line in range(3):
        for column in range(3):
            if grille_expected[line,column]!=grille_test[line,column]:
                condition=False
    return(condition)
    
def test_add_seed_to_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed, universe,x_start=1, y_start=1)
    test_equality=np.array(universe ==np.array([[0,0, 0, 0, 0, 0],
 [0, 0, 1, 1, 0, 0],
 [0, 1, 1, 0, 0, 0],
 [0 ,0, 1, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0]],dtype=np.uint8))
    assert test_equality.all()

def create_seed(type_seed):
    if type_seed=="r_pentomino":
        return(np.array([[0, 1, 1], [1, 1, 0], [0, 1, 0]]))


def add_seed_to_universe(seed,universe,x_start,y_start):
    abs,ord=seed.shape[0],seed.shape[1]
    for line in range(abs):
        for column in range(ord):
            universe[x_start+line][y_start+column]=seed[line][column]
    return(universe)
    
#ne pouvant pas utiliser pycharm car numpy ne s'importe pas je suis désolé mais nous ne pouvons faire la couverture de test, cependant nous avons tout testé jusqu'ici et nous vous invitons à le faire, taux de réussite de 100% pour le moment ;) 

## fonctionnalité 2

def afficher_univers(univers):
    plt.matshow(univers,cmap="gray_r")
    plt.show()
    return()

## fonctionnalité 3

seeds = { #nous avons pour le moment rajouté deux patterns
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "galaxy":[[1,1,1,1,1,1,0,1,1],[1,1,1,1,1,1,0,1,1],[0,0,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,0,0],[1,1,0,1,1,1,1,1,1],[1,1,0,1,1,1,1,1,1]],
    "blinker":[[0,0,0],[1,1,1],[0,0,0]],
    "toad":[[0,0,0,0],[0,1,1,1],[1,1,1,0],[0,0,0,0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}
from random import*

def generate_universe_v2(size=(15,15)): #on définit une valeur par défaut si l'utilisateur n'en rentre pas une
    print(seeds.keys()) #l'utilisateur voit le catalogue disponible
    key=input("écrivez une clef en minuscules") #il saisit sa clef
    seed=np.array(seeds[key])
    univers=generate_universe(size) #on génère l'univers
    try:
        grille=add_seed_to_universe(seed,univers,x_start=randint(0,univers.shape[0]-seed.shape[0]),y_start=randint(0,univers.shape[1]-seed.shape[1])) #on ré-utilise la fonction add... en plaçant notre nouvelle graine
    except:
        print("La grille choisie est trop petite") #on a ce message d'erreur si la grille est trop petite
    return(grille)


## Fonctionnalité 4
def environnement (x_cellule, y_cellule, univ):#Génère la somme des cellules environnant la cellule voulue.
    x_cellule,y_cellule=x_cellule+1,y_cellule+1 #on indente l'indiçage car on va simuler le tore par une univers auquel des bords correspondants aux lignes/colonnes opposées
    nb_line=univ.shape[0]
    nb_column=univ.shape[1]
    univers=np.zeros((univ.shape[0]+2,univ.shape[1]+2))#afin de simuler le tore on a donc un carré de taille n+2 où la 1ère ligne se retrouve en bas du nouvel univers, la dernière ligne elle se retrouve tout en haut, de même avec les colonnes
    for column in range(nb_column): #on fait ici l'opération annoncée ci-dessus
        univers[0][column+1]=univ[nb_line-1][column]
        univers[nb_line+1][column]=univ[0][column]
    for line in range(nb_line):
        univers[line+1][0]=univ[line][nb_column-1]
        univers[line+1][nb_column+1]=univ[line][0]
        
    for line in range(1,nb_line+1): #on colle ici dans le nouvel univers le pattern global de l'univers précédent
        for column in range(1,nb_column+1):
            univers[line,column]=univ[line-1,column-1]
    return (univers[x_cellule - 1][y_cellule - 1]+univers[x_cellule - 1][y_cellule]+univers[x_cellule - 1][y_cellule + 1] + univers[x_cellule][y_cellule - 1]+univers[x_cellule][y_cellule + 1] + univers[x_cellule + 1][y_cellule - 1]+univers[x_cellule + 1][y_cellule]+univers[x_cellule + 1][y_cellule + 1])


def survival (x_cellule, y_cellule,universe):
    voisin = environnement(x_cellule, y_cellule,universe)
    if voisin == 3 : #Dans tous les cas, si la cellule est entourée de 3 vivantes, elle survit.
        return 1
    elif voisin == 2 and universe[x_cellule][y_cellule] == 1 : #Si la cellule est vivante et entourée de deux vivantes, elle survit.
        return 1
    else :
        return 0


##Fonctionnalité 5

def generation (univers):
    new_univers = univers.copy() #On conserve la génération précédente.
    for x_coord in range(univers.shape[0]):
        for y_coord in range(univers.shape[1]):
            new_univers[x_coord][y_coord]=survival(x_coord,y_coord,univers) #On remplit cellule par cellule la nouvelle génération.
    return(new_univers)

def test_generation ():
    universe = np.array([[0,0,0,0,0,0],[0,0,0,1,0,0],[0,0,0,0,0,0],[0,0,0,0,1,1],[0,0,0,0,1,0],[0,0,0,0,0,0]])
    new_universe = np.array([[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,1,0],[0,0,0,0,1,1],[0,0,0,0,1,1],[0,0,0,0,0,0]]) #On génère une cellule, on en détruit une et on génère une cellule sur le bord.
    test_universe = generation(universe)
    return((new_universe == test_universe).all())

##Fonctionnalité 6
def game_life_simulate(univers, iteration):
    for age in range(iteration):
        univers = generation(univers)#On met à jour l'univers autant de fois qu'il y a d'intérations.
    return(univers)

def test_game_life_simulate ():
    universe = np.array([[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,0,0,0,0],[0,0,0,0,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]]) #On génère un beacon.
    new_universe = np.array([[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,1,0,0,0],[0,0,0,1,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]]) #Si le nombre d'itération est impaire, comme c'est un oscillateur d'ordre deux, on aura un autre motif.
    test_universe = game_life_simulate(universe,11)#Ici, le test doit être le nouveau motif.
    return((new_universe == test_universe).all())

## Fonctionnalité 7
from matplotlib.animation import FuncAnimation


"""
def gen_data(): #on génère notre univers
    global univers_evol
    univers_evol=generation(univers_evol)
    return(univers_evol)
        
def update(data): 
    mat.set_data(data)
    return(mat)
        
def data_gen():#on fait tourner l'algo
    while True: 
        yield gen_data()
        
fig,ax=plt.subplots()
univers_evol=game_life_simulate(generate_universe_v2(),0)  #on utilise notre fonction qui lance le jeu de la vie
mat=ax.matshow(gen_data(),cmap="gray_r") #on fait l'affichage  
anim=FuncAnimation(fig,update,data_gen,interval=500) #on anime l'image
plt.show()
"""

import imageio
def gif(liste_image, addresse):
    images = [] #va contenir les images pour le module imageio
    for filename in liste_image:
        images.append(imageio.imread(filename))#On ajoute les images dans la liste dédiée
    imageio.mimsave(addresse, images)#On enregistre le gif.


    
## Fonctionnalité 8 version 3
import imageio
def generate_universe_v3(tuple, key , seed_position):
    """Tuple est un tuple correspondant aux dimensions de l'univers. key est une str correspondant à la graine initiale. seed_position donne les coordonnées du point en haut à gauche de la graine."""
    seed=np.array(seeds[key])#on récupère la graine.
    univers=generate_universe(tuple) #on génère l'univers
    x_start = seed_position[0]
    y_start = seed_position[1]
    univers=add_seed_to_universe(seed,univers,x_start,y_start) #on ré-utilise la fonction add... en plaçant notre nouvelle graine
    return(univers)
    
def generation_v2 (univers):
    new_univers = univers.copy() #On conserve la génération précédente.
    for x_coord in range(univers.shape[0]):
        for y_coord in range(univers.shape[1]):
            new_univers[x_coord][y_coord]=survival(x_coord,y_coord,univers) #On remplit cellule par cellule la nouvelle génération.
    return(new_univers)

def affichage_bis(tuple, key , seed_position,dt=300,color='gray_r', iter = 30, Gif = False):
    """Tuple est un tuple correspondant aux dimensions de l'univers. key est une str correspondant à la graine initiale. seed_position donne les coordonnées du point en haut à gauche de la graine. dt, int, donne en milliseconde l'intervalle entre deux updates. color est une str donnant la couleur des cases. iter, int, donne le nombre d'itérations du processus. Gif est un booléen qui détermine si le programme va donne un GIF ou non. PS : cette dernière fonction n'est actuellement pas valide."""
    fig = plt.figure() #on génère la figure.
    univers = generate_universe_v3(tuple, key , seed_position) #On crée l'univers initial.
    image = [] #image regroupe tous les états successifs (but : créer le GIF)
    for gene in range(iter): #On itère le passage des générations.
        frame = plt.imshow(univers,cmap=color) #On affiche la grille.
        plt.pause(dt/1000) #On met le programme en pause pour voir la grille.
        univers = generation_v2(univers) #On fait progresser le processus.
        image.append(frame) #On ajoute cet état à la liste des états.
    if Gif:
        gif(image,'/Users/Théo/Documents.gif') #Création du gif
    plt.show()
    
##fonctionnalité 9 (nous sommes encore en train de chercher)

## fonctionnalité 10
"""
if __name__=='__main__':
    size=(int(input("nb lignes ?")),int(input("nb colonnes ?")))
    print(seeds.keys()) #l'utilisateur voit le catalogue disponible
    key=input("écrivez une clef en minuscules") #il saisit sa clef
    seed_position=(int(input("ligne de la seed?")),int(input("colonne de la seed?")))
    affichage(size,key,seed_position)
    """

## fonctionnalité 11
from tkinter import *
from functools import partial

## Fonctionnalité 8 - Tkinter, affichage de la grille de jeu
def couleur(ligne, colonne, univers):
    """Défini la couleur d'une case suivant sa valeur. univers est un tableau composé de 0 et de 1, et ligne, colonne sont des coordonées dans ce tableau."""
    if univers[ligne][colonne] == 0:#Case morte
        return 'white'
    else :#Case vivante
        return 'black'


def affichage_tk_grille(univers):
    largeur = univers.shape[1]
    hauteur = univers.shape[0]
    pas = 1000//univers.shape[0]#Le pas permet d'avoir des cellules visibles dans la fenêtre.
    root=Tk()#Création de la fenêtre de commande
    root.title("Game of life")
    game=Toplevel(root)
    game.title("Game of life")
    game.grid()
    can = Canvas(game, width = 1000, height = 1000)#Création de la grille où le jeu va évoluer.
    for ligne in range(hauteur):
        for colonne in range(largeur):
            can.create_rectangle(pas*colonne, pas*ligne,pas*( colonne + 1), pas*(ligne + 1),fill = couleur( ligne,colonne, univers ), width = 0)#On crée des cases dont la couleur dépend de l'état de la cellule dans le tableau univers. 
    can.grid()
    root.mainloop()
    return()

def affichage_tk_iter(tuple, key , seed_position,dt=300, iter = 30):
    """Tuple est un tuple correspondant aux dimensions de l'univers. key est une str correspondant à la graine initiale. seed_position donne les coordonnées du point en haut à gauche de la graine. dt, int, donne en milliseconde l'intervalle entre deux updates.  iter, int, donne le nombre d'itérations du processus.."""
    univers = generate_universe_v3(tuple, key , seed_position) #On crée l'univers initial.
    largeur = univers.shape[1]
    hauteur = univers.shape[0]
    pas = 1000//univers.shape[0]#Le pas permet d'avoir des cellules visibles dans la fenêtre.
    root=Tk()#Création de la fenêtre de commande
    root.title("Game of life")
    game=Toplevel(root)
    game.title("Game of life")
    game.grid()
    can = Canvas(game, width = 1000, height = 1000)#Création de la grille où le jeu va évoluer.
    for gene in range(iter): #On itère le passage des générations.
        for ligne in range(hauteur):
            for colonne in range(largeur):
                can.create_rectangle(pas*colonne, pas*ligne,pas*( colonne + 1), pas*(ligne + 1),fill = couleur( ligne,colonne, univers ), width = 0)#On crée des cases dont la couleur dépend de l'état de la cellule dans le tableau univers. 
        can.grid()
        can.update()
        plt.pause(dt/1000) #On met le programme en pause pour voir la grille.
        univers = generation_v2(univers) #On fait progresser le processus.
    root.mainloop()



##Fonctionnalité 9 : interface graphique dynamique
def affichage_tk_root(root,x_univ, y_univ, key , x_seed, y_seed,dt, iter):
    """Cette fonction, proche de affichage_tk_iter, en est une adaptation prévue pour interagir avec la fenêtre principale où sont les commandes. root est la fenêtre principale, key est une stringvar, dt est un boolvar,  iter est un intvar et les autres paramètres sont des données provenant de curseurs."""
    #On récupère le contenu des variables de la fenêtre root pour avoir des variables utilisables.
    key_str = key.get()
    tuple = (x_univ.get(), y_univ.get())
    seed_position = (x_seed.get(), y_seed.get())
    dt_bool = dt.get()#Suivant si la case est cochée ou non, on règle l'intervalle de mise à jour.
    if dt_bool :
        dt_int = 100
    else :
        dt_int = 10
    iter_int = iter.get()
    #On reprend la création du jeu de la vie sous Tkinter mais seulement si la graîne est bien placée dans l'univers.
    try :
        univers = generate_universe_v3(tuple, key_str , seed_position) #On crée l'univers initial.
        largeur = univers.shape[1]
        hauteur = univers.shape[0]
        pas = 1000//univers.shape[0]#Le pas permet d'avoir des cellules visibles dans la fenêtre.
        root.title("Game of life")
        game=Toplevel(root) # Création de la fenêtre où le jeu va évoluer.
        game.title("Game of life")
        game.grid()
        can = Canvas(game, width = 1000, height = 1000)#Création de la grille où le jeu va évoluer.
        for gene in range(iter_int): #On itère le passage des générations.
            for ligne in range(hauteur):
                for colonne in range(largeur):
                    can.create_rectangle(pas*colonne, pas*ligne,pas*( colonne + 1), pas*(ligne + 1),fill = couleur( ligne,colonne, univers ), width = 0)#On crée des cases dont la couleur dépend de l'état de la cellule dans le tableau univers. 
            can.grid()
            can.update()# Mise à jour du canevas.
            plt.pause(dt_int/1000) #On met le programme en pause pour voir la grille.
            univers = generation_v2(univers) #On fait progresser le processus.
    except : #Si la graîne est mal placée, on envoie un message d'erreur.
        erreur=Label(root, text = "La graîne n'est pas dans l'univers !", foreground = 'red' )
        erreur.grid(column=0, row=7)
        Button(root, text='Ok', command=partial(update_label, erreur, '')).grid(column=1, row=7)

def update_label(label, texte):
    label.config(text=texte)


def selection_seed(root,stringvar,label):
    """Permet la sélection d'une graîne parmis celles disponibles."""
    selection=Toplevel(root)#Création d'une nouvelle fenêtre.
    selection.title("Selection d'une graîne.")
    selection.grid()
    rang=0#Permet d'afficher les noms sous forme d'une liste.
    for key in list_key:
        Button(selection, text = key, command = partial(write,selection,  key ,stringvar, label )).grid(column=0,row=rang)#Un bouton par graîne.
        rang += 1

def write(root, key, stringvar, label):
    """Permet de récupérer la valeur de la graîne séléctionnée et de fermer la fenêtre annexe."""
    stringvar.set(key)#On récupère le type de graîne.
    text = stringvar.get()
    label.config(text=text)#On affiche dans la fenêtre principale le nom de la graîne sélectionnée.
    root.destroy()#On ferme la fenêtre de sélection des graînes.
    

def affichage_tk_reglages():
    """Affiche une fenêtre de commande permettant de lancer le jeu de la vie."""
    root=Tk()#Création de la fenêtre de commande
    root.title("Game of life")
    
    #Acquisition des données à l'aide de curseurs, de champs de saisie et de fenêtre annexe.
    #Ici, on gère les curseurs pour la taille de l'univers et pour coordonnées de la graîne.
    x_univ = Scale(root, orient=HORIZONTAL, label ='Hauteur :', troughcolor ='dark grey', resolution=5, sliderlength =20, showvalue =1, from_=15, to=50)
    y_univ = Scale(root, orient=HORIZONTAL, label ='Largeur :', troughcolor ='dark grey', resolution=5, sliderlength =20, showvalue =1, from_=15, to=50) 
    x_seed = Scale(root, orient=HORIZONTAL, label ='Abs de la graîne :', troughcolor ='dark grey', resolution=1, sliderlength =20, showvalue =1, from_=0, to=50)
    y_seed = Scale(root, orient=HORIZONTAL, label ='Ord de la graîne :', troughcolor ='dark grey', resolution=1, sliderlength =20, showvalue =1, from_=0, to=50)
    #Ici, on récupère le type de graîne.
    key = StringVar(root)
    label_key = Label(root, text = "")
    button_key = Button(root, text = "Type de graîne", command=partial(selection_seed,root,key,label_key))
    #Ici, on demande de sélectionner l'intervalle de mise à jour et le nombre de générations.
    dt = BooleanVar(root)
    label_dt = Label(root, text="Ralentir la mise à jour :")
    checkbox_dt = Checkbutton(root, variable=dt)
    iter = IntVar(root)
    label_iter = Label(root, text='Nombre de génération :')
    entry_iter = Entry(root, textvariable=iter)
    
    #Lancer l'animation.
    button = Button(root, text='Lancer le modèle',command=partial(affichage_tk_root,root,x_univ, y_univ, key , x_seed, y_seed,dt, iter))#Lance l'animation avec les fonctions précédemment définies.
    bouton = Button(root, text = 'Quitter', command = root.destroy)#Permet de quitter l'interface.
    
    #Gérer les positions.
    Label(root, text='Bienvenue dans le simulateur du jeu de la vie.', foreground = 'blue').grid(column = 0, columnspan=2 , row = 0)
    x_univ.grid(column = 0 , row=1)
    y_univ.grid(column = 1 , row=1)
    x_seed.grid(column = 0, row=3)
    y_seed.grid(column=1,row=3)
    button_key.grid(column=0, row=2)
    label_key.grid(column=1, row=2)
    label_dt.grid(column=0, row=4)
    checkbox_dt.grid(column=1, row=4)
    label_iter.grid(column=0, row=5)
    entry_iter.grid(column=1, row=5)
    button.grid(column = 0 , row = 6)
    bouton.grid(column = 1, row=6)
    root.mainloop()

seeds = { #nous avons pour le moment rajouté deux patterns
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "galaxy":[[1,1,1,1,1,1,0,1,1],[1,1,1,1,1,1,0,1,1],[0,0,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,0,0],[1,1,0,1,1,1,1,1,1],[1,1,0,1,1,1,1,1,1]],
    "blinker":[[0,0,0],[1,1,1],[0,0,0]],
    "toad":[[0,0,0,0],[0,1,1,1],[1,1,1,0],[0,0,0,0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}

list_key = [i for i in seeds] #liste des noms de graînes.




##9
'''import argparse
parser = argparse.ArgumentParser()
parser.add_argument('tuple')
parser.add_argument('key')
parser.add_argument('seed_position')
parser.add_argument('dt')
parser.add_argument('color')
parser.add_argument('iter')
args = parser.parse_args()
print(args.tuple)'''



## sprint 4  - Pygame

import copy
import pygame
from pygame import *
import random

#constantes
red = (255, 0, 0)
black = (0, 0, 0)
white = (255, 255, 255)
#coordonnées des voisins
neighbours = [[-1,-1],[-1,0],[-1,+1],
              [0,-1],        [0,+1],   
              [+1,-1],[+1,0],[+1,+1],]
                
"""cellules=[[i for i in range(50)] for i in range(50)]"""

#Fonction de jeu en pygame        
def play():                
        #initialization
        pygame.init()
        scrn = pygame.display.set_mode((500, 500))#on ouvre une fenêtre de la taille souhaitée
        mainsrf = pygame.Surface((500, 500))
        mainsrf.fill(white) #on prend un fond blanc
        univers=generate_universe_v2()#on initialise notre univers à l'aide de la fonction que nous avons créée, on doit juste prendre le temps dans la console de spécifier la seed désirée
        
        while 1:
                univers=generation_v2(univers)# on update grâce à notre fonction l'univers de jeu
                #boucle préventive qui gère le cas où l'utilisateur souhaite quitter le jeu
                for event in pygame.event.get():
                        if event.type == QUIT:
                                pygame.quit()
                                """sys.exit()""" #permet d'éviter d'avoir kernel process exited 
                #partie traçage de l'algorithme 
                for y in range(univers.shape[0]):
                        for x in range(univers.shape[1]):
                                if univers[x][y]==1: #si cellule vivante
                                        pygame.draw.rect(mainsrf, red, (x*10, y*10, 10, 10))
                                else:#si morte
                                        pygame.draw.rect(mainsrf, white, (x*10, y*10, 10, 10))
                scrn.blit(mainsrf, (0, 0))
                pygame.display.update()