## fonctionnalité 11
#from game_of_life_theo_G_françois_BL import *

from gameoflife_tg_fbl_partie1_matplotlib import*
from gameoflife_tg_fbl_partie2_matplotlib import*


from tkinter import *
from functools import partial

## Fonctionnalité 8 - Tkinter, affichage de la grille de jeu
def couleur(ligne, colonne, univers):
    """Défini la couleur d'une case suivant sa valeur. univers est un tableau composé de 0 et de 1, et ligne, colonne sont des coordonées dans ce tableau."""
    if univers[ligne][colonne] == 0:#Case morte
        return 'white'
    else :#Case vivante
        return 'black'


def affichage_tk_grille(univers):
    largeur = univers.shape[1]
    hauteur = univers.shape[0]
    pas = 1000//univers.shape[0]#Le pas permet d'avoir des cellules visibles dans la fenêtre.
    root=Tk()#Création de la fenêtre de commande
    root.title("Game of life")
    game=Toplevel(root)
    game.title("Game of life")
    game.grid()
    can = Canvas(game, width = 1000, height = 1000)#Création de la grille où le jeu va évoluer.
    for ligne in range(hauteur):
        for colonne in range(largeur):
            can.create_rectangle(pas*colonne, pas*ligne,pas*( colonne + 1), pas*(ligne + 1),fill = couleur( ligne,colonne, univers ), width = 0)#On crée des cases dont la couleur dépend de l'état de la cellule dans le tableau univers. 
    can.grid()
    root.mainloop()
    return()

def affichage_tk_iter(tuple, key , seed_position,dt=300, iter = 30):
    """Tuple est un tuple correspondant aux dimensions de l'univers. key est une str correspondant à la graine initiale. seed_position donne les coordonnées du point en haut à gauche de la graine. dt, int, donne en milliseconde l'intervalle entre deux updates.  iter, int, donne le nombre d'itérations du processus.."""
    univers = generate_universe_v3(tuple, key , seed_position) #On crée l'univers initial.
    largeur = univers.shape[1]
    hauteur = univers.shape[0]
    pas = 1000//univers.shape[0]#Le pas permet d'avoir des cellules visibles dans la fenêtre.
    root=Tk()#Création de la fenêtre de commande
    root.title("Game of life")
    game=Toplevel(root)
    game.title("Game of life")
    game.grid()
    can = Canvas(game, width = 1000, height = 1000)#Création de la grille où le jeu va évoluer.
    for gene in range(iter): #On itère le passage des générations.
        for ligne in range(hauteur):
            for colonne in range(largeur):
                can.create_rectangle(pas*colonne, pas*ligne,pas*( colonne + 1), pas*(ligne + 1),fill = couleur( ligne,colonne, univers ), width = 0)#On crée des cases dont la couleur dépend de l'état de la cellule dans le tableau univers. 
        can.grid()
        can.update()
        plt.pause(dt/1000) #On met le programme en pause pour voir la grille.
        univers = generation_v2(univers) #On fait progresser le processus.
    root.mainloop()



##Fonctionnalité 9 : interface graphique dynamique
def affichage_tk_root(root,x_univ, y_univ, key , x_seed, y_seed,dt, iter):
    """Cette fonction, proche de affichage_tk_iter, en est une adaptation prévue pour interagir avec la fenêtre principale où sont les commandes. root est la fenêtre principale, key est une stringvar, dt et iter sont des intvar et les autres paramètres sont des données provenant de curseurs."""
    #On récupère le contenu des variables de la fenêtre root pour avoir des variables utilisables.
    key_str = key.get()
    tuple = (x_univ.get(), y_univ.get())
    seed_position = (x_seed.get(), y_seed.get())
    dt_int = dt.get()
    iter_int = iter.get()
    #On reprend la création du jeu de la vie sous Tkinter mais seulement si la graîne est bien placée dans l'univers.
    try :
        univers = generate_universe_v3(tuple, key_str , seed_position) #On crée l'univers initial.
        largeur = univers.shape[1]
        hauteur = univers.shape[0]
        pas = 1000//univers.shape[0]#Le pas permet d'avoir des cellules visibles dans la fenêtre.
        root.title("Game of life")
        game=Toplevel(root) # Création de la fenêtre où le jeu va évoluer.
        game.title("Game of life")
        game.grid()
        can = Canvas(game, width = 1000, height = 1000)#Création de la grille où le jeu va évoluer.
        for gene in range(iter_int): #On itère le passage des générations.
            for ligne in range(hauteur):
                for colonne in range(largeur):
                    can.create_rectangle(pas*colonne, pas*ligne,pas*( colonne + 1), pas*(ligne + 1),fill = couleur( ligne,colonne, univers ), width = 0)#On crée des cases dont la couleur dépend de l'état de la cellule dans le tableau univers. 
            can.grid()
            can.update()# Mise à jour du canevas.
            plt.pause(dt_int/1000) #On met le programme en pause pour voir la grille.
            univers = generation_v2(univers) #On fait progresser le processus.
    except : #Si la graîne est mal placée, on envoie un message d'erreur.
        Label(root, text = "La graîne n'est pas dans l'univers !", foreground = 'red' ).grid(column=0, row=7)

def selection_seed(root,stringvar,label):
    """Permet la sélection d'une graîne parmis celles disponibles."""
    selection=Toplevel(root)#Création d'une nouvelle fenêtre.
    selection.title("Selection d'une graîne.")
    selection.grid()
    rang=0#Permet d'afficher les noms sous forme d'une liste.
    for key in list_key:
        Button(selection, text = key, command = partial(write,selection,  key ,stringvar, label )).grid(column=0,row=rang)#Un bouton par graîne.
        rang += 1

def write(root, key, stringvar, label):
    """Permet de récupérer la valeur de la graîne séléctionnée et de fermer la fenêtre annexe."""
    stringvar.set(key)#On récupère le type de graîne.
    text = stringvar.get()
    label.config(text=text)#On affiche dans la fenêtre principale le nom de la graîne sélectionnée.
    root.destroy()#On ferme la fenêtre de sélection des graînes.
    

def affichage_tk_reglages():
    """Affiche une fenêtre de commande permettant de lancer le jeu de la vie."""
    root=Tk()#Création de la fenêtre de commande
    root.title("Game of life")
    
    #Acquisition des données à l'aide de curseurs, de champs de saisie et de fenêtre annexe.
    #Ici, on gère les curseurs pour la taille de l'univers et pour coordonnées de la graîne.
    x_univ = Scale(root, orient=HORIZONTAL, label ='Hauteur :', troughcolor ='dark grey', resolution=5, sliderlength =20, showvalue =1, from_=15, to=50)
    y_univ = Scale(root, orient=HORIZONTAL, label ='Largeur :', troughcolor ='dark grey', resolution=5, sliderlength =20, showvalue =1, from_=15, to=50) 
    x_seed = Scale(root, orient=HORIZONTAL, label ='Abs de la graîne :', troughcolor ='dark grey', resolution=1, sliderlength =20, showvalue =1, from_=0, to=50)
    y_seed = Scale(root, orient=HORIZONTAL, label ='Ord de la graîne :', troughcolor ='dark grey', resolution=1, sliderlength =20, showvalue =1, from_=0, to=50)
    #Ici, on récupère le type de graîne.
    key = StringVar(root)
    label_key = Label(root, text = "")
    button_key = Button(root, text = "Type de graîne", command=partial(selection_seed,root,key,label_key))
    #Ici, on demande de sélectionner l'intervalle de mise à jour et le nombre de générations.
    dt = IntVar(root)
    label_dt = Label(root, text="Intervalle d'update (en ms) :")
    entry_dt = Entry(root, textvariable=dt)
    iter = IntVar(root)
    label_iter = Label(root, text='Nombre de génération :')
    entry_iter = Entry(root, textvariable=iter)
    
    #Lancer l'animation.
    button = Button(root, text='Lancer le modèle',command=partial(affichage_tk_root,root,x_univ, y_univ, key , x_seed, y_seed,dt, iter))#Lance l'animation avec les fonctions précédemment définies.
    bouton = Button(root, text = 'Quitter', command = root.destroy)#Permet de quitter l'interface.
    
    #Gérer les positions.
    Label(root, text='Bienvenue dans le simulateur du jeu de la vie.', foreground = 'blue').grid(column = 0, columnspan=2 , row = 0)
    x_univ.grid(column = 0 , row=1)
    y_univ.grid(column = 1 , row=1)
    x_seed.grid(column = 0, row=3)
    y_seed.grid(column=1,row=3)
    button_key.grid(column=0, row=2)
    label_key.grid(column=1, row=2)
    label_dt.grid(column=0, row=4)
    entry_dt.grid(column=1, row=4)
    label_iter.grid(column=0, row=5)
    entry_iter.grid(column=1, row=5)
    button.grid(column = 0 , row = 6)
    bouton.grid(column = 1, row=6)
    root.mainloop()

seeds = { #nous avons pour le moment rajouté deux patterns
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "galaxy":[[1,1,1,1,1,1,0,1,1],[1,1,1,1,1,1,0,1,1],[0,0,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,0,0],[1,1,0,1,1,1,1,1,1],[1,1,0,1,1,1,1,1,1]],
    "blinker":[[0,0,0],[1,1,1],[0,0,0]],
    "toad":[[0,0,0,0],[0,1,1,1],[1,1,1,0],[0,0,0,0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}

list_key = [i for i in seeds] #liste des noms de graînes.