import numpy as np
import matplotlib.pyplot as plt

## test_generate_universe


from pytest import *


def test_generate_universe():
    condition=True
    grille_expected=np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]])
    grille_test=generate_universe((4,4))
    for line in range(4):
        for column in range(4):
            if grille_expected[line,column]!=grille_test[line,column]:
                condition=False
    return(condition)
    
## generate_universe

def generate_universe(size): #size est un tuple
    universe=np.zeros(size)
    return(universe)
    
## test pentomino

def test_create_seed(): #nous n'avions pas compris à l'époque la méthode .all()
    
    condition=True
    grille_expected=np.array([[0, 1, 1], [1, 1, 0], [0, 1, 0]])
    grille_test=create_seed(type_seed="r_pentomino")
    for line in range(3):
        for column in range(3):
            if grille_expected[line,column]!=grille_test[line,column]:
                condition=False
    return(condition)
    
def test_add_seed_to_universe():
    seed = create_seed(type_seed = "r_pentomino")
    universe = generate_universe(size=(6,6))
    universe = add_seed_to_universe(seed, universe,x_start=1, y_start=1)
    test_equality=np.array(universe ==np.array([[0,0, 0, 0, 0, 0],
 [0, 0, 1, 1, 0, 0],
 [0, 1, 1, 0, 0, 0],
 [0 ,0, 1, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0],
 [0 ,0, 0, 0, 0, 0]],dtype=np.uint8))
    assert test_equality.all()

def create_seed(type_seed):
    if type_seed=="r_pentomino":
        return(np.array([[0, 1, 1], [1, 1, 0], [0, 1, 0]]))


def add_seed_to_universe(seed,universe,x_start,y_start):
    abs,ord=seed.shape[0],seed.shape[1]
    for line in range(abs):
        for column in range(ord):
            universe[x_start+line][y_start+column]=seed[line][column]
    return(universe)
    
#ne pouvant pas utiliser pycharm car numpy ne s'importe pas je suis désolé mais nous ne pouvons faire la couverture de test, cependant nous avons tout testé jusqu'ici et nous vous invitons à le faire, taux de réussite de 100% pour le moment ;) 

## fonctionnalité 2

def afficher_univers(univers):
    plt.matshow(univers,cmap="gray_r")
    plt.show()
    return()

## fonctionnalité 3

seeds = { #nous avons pour le moment rajouté deux patterns
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "galaxy":[[1,1,1,1,1,1,0,1,1],[1,1,1,1,1,1,0,1,1],[0,0,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,1,1],[1,1,0,0,0,0,0,0,0],[1,1,0,1,1,1,1,1,1],[1,1,0,1,1,1,1,1,1]],
    "blinker":[[0,0,0],[1,1,1],[0,0,0]],
    "toad":[[0,0,0,0],[0,1,1,1],[1,1,1,0],[0,0,0,0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}
from random import*

def generate_universe_v2(size=(20,20)): #on définit une valeur par défaut si l'utilisateur n'en rentre pas une
    print(seeds.keys()) #l'utilisateur voit le catalogue disponible
    key=input("écrivez une clef en minuscules") #il saisit sa clef
    seed=np.array(seeds[key])
    univers=generate_universe(size) #on génère l'univers
    try:
        grille=add_seed_to_universe(seed,univers,x_start=randint(0,univers.shape[0]-seed.shape[0]),y_start=randint(0,univers.shape[1]-seed.shape[1])) #on ré-utilise la fonction add... en plaçant notre nouvelle graine
    except:
        print("La grille choisie est trop petite") #on a ce message d'erreur si la grille est trop petite
    return(grille)


## Fonctionnalité 4
def environnement (x_cellule, y_cellule, univ):#Génère la somme des cellules environnant la cellule voulue.
    x_cellule,y_cellule=x_cellule+1,y_cellule+1 #on indente l'indiçage car on va simuler le tore par une univers auquel des bords correspondants aux lignes/colonnes opposées
    nb_line=univ.shape[0]
    nb_column=univ.shape[1]
    univers=np.zeros((univ.shape[0]+2,univ.shape[1]+2))#afin de simuler le tore on a donc un carré de taille n+2 où la 1ère ligne se retrouve en bas du nouvel univers, la dernière ligne elle se retrouve tout en haut, de même avec les colonnes
    for column in range(nb_column): #on fait ici l'opération annoncée ci-dessus
        univers[0][column+1]=univ[nb_line-1][column]
        univers[nb_line+1][column]=univ[0][column]
    for line in range(nb_line):
        univers[line+1][0]=univ[line][nb_column-1]
        univers[line+1][nb_column+1]=univ[line][0]
        
    for line in range(1,nb_line+1): #on colle ici dans le nouvel univers le pattern global de l'univers précédent
        for column in range(1,nb_column+1):
            univers[line,column]=univ[line-1,column-1]
    return (univers[x_cellule - 1][y_cellule - 1]+univers[x_cellule - 1][y_cellule]+univers[x_cellule - 1][y_cellule + 1] + univers[x_cellule][y_cellule - 1]+univers[x_cellule][y_cellule + 1] + univers[x_cellule + 1][y_cellule - 1]+univers[x_cellule + 1][y_cellule]+univers[x_cellule + 1][y_cellule + 1])


def survival (x_cellule, y_cellule,universe):
    voisin = environnement(x_cellule, y_cellule,universe)
    if voisin == 3 : #Dans tous les cas, si la cellule est entourée de 3 vivantes, elle survit.
        return 1
    elif voisin == 2 and universe[x_cellule][y_cellule] == 1 : #Si la cellule est vivante et entourée de deux vivantes, elle survit.
        return 1
    else :
        return 0


##Fonctionnalité 5

def generation (univers):
    new_univers = univers.copy() #On conserve la génération précédente.
    for x_coord in range(univers.shape[0]):
        for y_coord in range(univers.shape[1]):
            new_univers[x_coord][y_coord]=survival(x_coord,y_coord,univers) #On remplit cellule par cellule la nouvelle génération.
    return(new_univers)

def test_generation ():
    universe = np.array([[0,0,0,0,0,0],[0,0,0,1,0,0],[0,0,0,0,0,0],[0,0,0,0,1,1],[0,0,0,0,1,0],[0,0,0,0,0,0]])
    new_universe = np.array([[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,1,0],[0,0,0,0,1,1],[0,0,0,0,1,1],[0,0,0,0,0,0]]) #On génère une cellule, on en détruit une et on génère une cellule sur le bord.
    test_universe = generation(universe)
    return((new_universe == test_universe).all())

##Fonctionnalité 6
def game_life_simulate(univers, iteration):
    for age in range(iteration):
        univers = generation(univers)#On met à jour l'univers autant de fois qu'il y a d'intérations.
    return(univers)

def test_game_life_simulate ():
    universe = np.array([[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,0,0,0,0],[0,0,0,0,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]]) #On génère un beacon.
    new_universe = np.array([[0,0,0,0,0,0],[0,1,1,0,0,0],[0,1,1,0,0,0],[0,0,0,1,1,0],[0,0,0,1,1,0],[0,0,0,0,0,0]]) #Si le nombre d'itération est impaire, comme c'est un oscillateur d'ordre deux, on aura un autre motif.
    test_universe = game_life_simulate(universe,11)#Ici, le test doit être le nouveau motif.
    return((new_universe == test_universe).all())